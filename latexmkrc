$lualatex = 'lualatex --recorder --shell-escape -halt-on-error %O %S && cp %D ./%R.pdf';
$pdflatex = 'pdflatex --recorder --shell-escape -halt-on-error %O %S && cp %D ./%R.pdf';
$pdf_mode = 4;
$postscript_mode = $dvi_mode = 0;
$out_dir = ".build";

#add_cus_dep('ntn', 'not', 0, 'makeglossaries');
add_cus_dep('slo', 'sls', 0, 'makeglossaries');
sub makeglossaries {
	$dir = dirname($_[0]);
	$file = basename($_[0]);
	if ( $silent ) {
		system "makeglossaries -q -d '$dir' '$file'";
	} else {
		system "makeglossaries -d '$dir' '$file'";
	};
}

#push @generated_exts, 'nlg', 'not', 'ntn';
push @generated_exts, 'slg', 'sls', 'slo';
$clean_ext .= ' %R.ist %R.xdy';

$ENV{'max_print_line'}=1000;
$ENV{'error_line'}=254;
$ENV{'half_error_line'}=238;

#$ENV{'TEXINPUTS'}='./packages//:' . $ENV{'TEXINPUTS'};
$ENV{'TEXINPUTS'}='./packages//:';
