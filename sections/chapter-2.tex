\chapter{Darstellungstheorie I}
\label{chapter:representation-theory}
\setStdField{K}

Wie aus der Kategorientheorie bekannt sein mag, liefert das Yoneda"=Lemma die Interpretation, nicht eine Struktur selbst, sondern \enquote{ihre Freunde} bzw. \enquote{ihr Verhalten} zu studieren.
In anderen Bereichen der Algebra hat sich das bereits als fruchtbar gezeigt.
Untersucht werden dabei nicht nur Morphismen, sondern auch Strukturen, auf denen die eigentlich untersuchte Struktur operiert.
Beispiele hierfür liefern in der Gruppentheorie die \(G\)-Mengen, aber auch die Moduln und Vektorräume bezüglich Ringen bzw. Körpern.
Hierdurch konnte beispielsweise der Struktursatz abelscher Gruppen bewiesen werden.

Wir wollen dieses Konzept nun auch bei Lie"=Algebren anwenden.
Dies wird sich als sehr ergiebig erweisen, da unsere vorherigen Resultate einige Verallgemeinerungen erfahren werden.
Insbesondere erhalten wir dadurch viele neue Werkzeuge für den Umgang mit Lie-Algebren.

\section{Darstellungen und Moduln}
Sei \(L\) eine Lie-Algebra über einem Körper \(K\).

\begin{definition}
	Sei \(V\) ein \(K\)-Vektorraum.
	Dann heißt \(V\) \Def{\(L\)-Modul}, falls eine Abbildung
	\[L \times V \to V, (x,v) \mapsto x \cdot v\]
	existiert, so dass für \(x, y \in L\), \(v, w \in V\), \(a, b \in K\)
	\begin{enumerate}
		\item \((ax + by) \cdot v = a(x \cdot v) + b(y \cdot v)\)
		\item \(x \cdot (av + bw) = a(x \cdot v) + b(x \cdot w)\)
		\item \([x,y] \cdot v = x \cdot (y \cdot v) - y \cdot (x \cdot v)\)
	\end{enumerate}
\end{definition}
Ist \(V\) ein \(L\)-Modul, dann ist für alle \(x \in L\)
\[ \gl{V} \ni \phi(x): V \to V, ~ v \mapsto x \cdot v \]
und \(\phi: L \to \gl{V}, ~ x \mapsto \phi(x)\) eine Darstellung von \(L\) (vgl. \cref{def:basic-structure-names}.9).
Ist \(\phi: V \to \gl{V}\) Darstellung, dann ist \(V\) ein \(L\)-Modul mit \(x \cdot v \coloneq \phi(x)(v)\) für \(x \in L\), \(v \in V\).
Darstellungen von \(L\) entsprechen also gerade den \(L\)-Moduln.
Wir bezeichnen mit \Def{\(\LieModules\)} die Kategorie der \(L\)-Moduln und verwenden die gleichen Konzepte, wie in der Darstellungstheorie assoziativer Algebren, z.B: Untermoduln, halbeinfache, einfache Moduln, etc.
\begin{bemerkung}
	\label{thm:lie-hom-modules}
	Seien \(V, W \in \LieModules\).
	\begin{enumerate}
		\item \(\Dual{V} = \Hom{V}{K}\) ist \(L\)-Modul mit
			\[ (x \cdot f)(v) \coloneq -f(x \cdot v), \qquad \text{mit } f \in \Dual{V}, x \in L, v \in V\]
		\item \(\Hom{V}{W}\) ist \(L\)-Modul mit
			\[ (x \cdot f)(v) \coloneq x \cdot f(v) - f(x \cdot v), \qquad \text{mit } f \in \Hom{V}{W}, x \in L, v \in V \]
	\end{enumerate}
\end{bemerkung}
\begin{beweis}
	\begin{enumerate}
		\item Folgt aus (2), wobei auf dem Körper trivial operiert werde.
		\item \textcolor{red}{Übung}.
	\end{enumerate}
\end{beweis}
\begin{definitionen}
	Seien \(V,W \in \LieModules\).
	\begin{enumerate}
		\item Ein Untervektorraum \(U \subseteq V\) heißt \Def{Untermodul} \(U \SubLieMod V\) von \(V\), wenn gilt
			\[ L \cdot U = \br{ x \cdot u : x \in L, u \in U } \subseteq U .\]
		\item \(V\) heißt \Def{einfach}, wenn \(V \neq 0\) und \(0, V\) die einzigen Untermoduln sind.
		\item \(V\) heißt \Def{halbeinfach}, wenn \(V\) endlich"=dimensional ist und
			\[ V = V_1 \oplus \cdots \oplus V_t \]
			mit einfachen Untermoduln \(V_i\) für \(1 \leq i \leq t\) ist.
		\item \(\alpha \in \Hom{V}{W}\) heißt \Def{\(L\)-Modul-Homomorphismus} oder \Def{\(L\)-linear}, wenn für alle \(x \in L\) und \(v \in V\) stets \( \alpha(x \cdot v) = x \cdot \alpha(v) \) gilt.
			\(\Hom[L]{V}{W}\) ist die Menge dieser Abbildungen.
	\end{enumerate}
\end{definitionen}
Wie auch schon bei den Lie-Algebren, wollen wir diese Begriffe weiter charakterisieren.
So lässt sich \(L\)-Linearität von \(f \in \Hom{V}{W}\) durch Nachrechnen von \(x \cdot f = 0\) für alle \(x \in L\) zeigen, denn dann ist für \(v \in V\)
\[ 0 = (x \cdot f)(v) = x \cdot f(v) - f(x \cdot v) \quad\iff\quad x \cdot f(v) = f(x \cdot v) .\]
Es gelten ferner die üblichen Zusammenhänge:
\begin{bemerkung}
	Sei \(L\) eine Lie-Algebra, \(V, W \in \LieModules\), \(U, I \SubLieMod V\) Untermoduln und \(\alpha \in \Hom[L]{V}{W}\) \(L\)-Modul-Homomorphismus.
	\begin{enumerate}
		\item
			\(V / U\) wird \(L\)-Modul durch
			\[ x \cdot (v + U) \coloneq x \cdot v + U \]
			für \(x \in L\) und \(v \in V\).
		\item Es sind \(\Image{\alpha}\) und \(\Kernel{\alpha}\) je Untermoduln.
		\item Es gelten die Homomorphiesätze:
			\begin{enumerate}
				\item Sei \(\alpha: V \to W\) \(L\)-Modul-Homomorphismus.
					Dann gilt
					\[ V/\Kernel{\alpha} \LieModIso \Image{\alpha} \]
				\item Auch \(U + I, U \cap I \SubLieMod U\) sind Untermoduln und es gilt
					\[ (U + I)/I \LieModIso U/(U \cap I) \]
			\end{enumerate}
		\item Es wird \(L\) selbst \(L\)-Modul durch die \Def{adjungierte Darstellung}
			\[ x \cdot v \coloneq [x,v] \]
			für \(x, v \in L\).
			Die Untermoduln sind gerade die Ideale von \(L\) und \(L\) ist als \(L\)-Modul genau dann einfach und \(L \cdot L = [L,L] \neq 0\), wenn \(L\) einfach als Lie-Algebra ist.
			Wenn \(L\) als \(L\)-Modul halbeinfach und kein von \(0\) verschiedenes Ideal abelsch ist, dann ist \(L\) halbeinfache Lie-Algebra.
			Unter den Vorraussetzungen von \cref{thm:characterisation-of-semisimple-lie-algebras} gilt auch die Umkehrung.
	\end{enumerate}
\end{bemerkung}
Wir geben nun ein intuitives Kriterium für Halbeinfachheit an.
\begin{satz}
	\label{thm:semisimple-module-criterion}
	Sei \(V \in \LieModules\) endlich"=dimensional.
	Genau dann ist \(V\) halbeinfach, wenn für alle \(W \SubLieMod V\) ein \(W' \SubLieMod V\) mit \(V = W \oplus W'\) existiert.
	Das heißt, genau dann, wenn jeder Untermodul ein Komplement besitzt.
\end{satz}
\begin{beweis}
	\begin{itemize}
	\item[\enquote{\(\Rightarrow\)}:]
		Wähle \(W' \SubLieMod V\) maximal mit \(W \cap W' = 0\).
		Angenommen, \(V \neq W \oplus W'\).
		Nach Definition ist \(V = V_1 \oplus \cdots \oplus V_t\) mit einfachen \(L\)-Moduln \(V_i\).
		Dann existiert ein \(i\) mit \(V_i \NotSubLieMod W \oplus W'\), weshalb \(V_i \cap (W \oplus W') \neq V_i\) sein muss.
		Da \(V_i\) einfach ist, folgt also \(V_i \cap (W \oplus W') = 0\).
		Wir erhalten den Widerspruch \((V_i \oplus W') \cap W = 0\) wegen der Wahl von \(W'\).
	\item[\enquote{\(\Leftarrow\)}:]
		Sei \(V_1 \oplus \cdots \oplus V_s \SubLieMod V\) direkte Summe einfacher Untermoduln, wobei \(s\) maximal sein soll.
		Dann existiert \(W' \SubLieMod V\) mit \(V = (V_1 \oplus \cdots \oplus V_s) \oplus W'\).
		Ist \(W' \neq 0\), so existiert ein einfacher Untermodul \(V_{s+1} \SubLieMod W'\) und \(V_1 \oplus \cdots \oplus V_s \oplus V_{s+1}\) ist eine direkte Summe, Widerspruch.
	\end{itemize}
\end{beweis}
Um obiges Kriterium zu überpüfen, bietet es sich oft an, Modul-Endomorphismen zu untersuchen.
Insbesondere sind wir dabei am Kern interessiert.
Das folgende, leicht zu prüfende, aber nicht unbedingt intuitive, Resultat liefert eine Aussage darüber, was nicht Teil vom Kern eines Endomorphismus ist.
\begin{lemma}[Schur]
	\label{thm:schurs-lemma}
	Wenn \(K\) algebraisch abgeschlossen und \(V\) endlich"=dimensionaler einfacher \(L\)-Modul sind, dann ist
	\[ \End[L]{V} = \{ a \cdot \Id[V] : a \in K \} .\]
\end{lemma}
\begin{beweis}
	Sei \(a \in K\) Eigenwert von \(\phi \in \End[L]{V}\).
	Es gilt \(\phi - a \cdot \Id[V] \in \End[L]{V}\) und es ist \(\Kernel{(\phi - a \cdot \Id[V])} \neq 0\).
	Da \(V\) einfach ist, gilt \(\Kernel{(\phi - a \cdot \Id[V])} = V\) und daher \(\phi = a \cdot \Id[V]\).
\end{beweis}

\section{Das Casimir-Element einer Darstellung}
Sei \(L\) endlich-dimensionale Lie-Algebra über dem Körper \(K\) und \(V\) ein endlich"=dimensionaler \(L\)-Modul mit zugehöriger Darstellung \(\phi\).

Für den Satz von Weyl werden wir eine \(L\)-lineare Abbildung benötigen, welche von \(\phi\) herrührt.
Im Allgemeinen sind die Elemente im Bild von \(\phi\) nämlich nur \(K\)-linear.
Der folgende Endomorphismus eignet sich besonders, da seine Spur zugleich Dimensionen einfängt.
Im Wesentlichen liegt das an der Dualbasis, welche wir für die Konstruktion verwenden.
\begin{lemma}
	\label{thm:casimir-element}
	Sei \(\beta: L \times L \to K\) symmetrische, assoziative (d.h. \(\beta([x,y],z) = \beta(x,[y,z])\)), nicht-ausgeartete Bilinearform.
	Zu dieser seien nun \(\{x_1, \ldots, x_n\}\), \(\{y_1, \ldots, y_n\}\) duale Basen von \(L\), d.h. \(\beta(x_i,-) = \Dual{y_i}\) bzw.\ \(\beta(x_i,y_j) = \Kronecker{i}{j}\).
	Dann ist
	\[ \Casimir(\beta) \coloneq \sum_{i=1}^{n} \phi(x_i) \circ \phi(y_i) \in \End[L]{V} \]
	ein \(L\)-linearer \(V\)-Endomorphismus.
\end{lemma}
\begin{beweis}
	Sei \(c \coloneq \Casimir(\beta) \in \End{V}\).
	Es gilt \([\phi(x),c] = 0\) für alle \(x \in L\) zu zeigen, denn
	\begin{align*}
		& c \in \End[L]{V} \\
		\iff & c(x \cdot v) = c(\phi(x)(v)) = \phi(x)(c(v)) = x \cdot c(v) \quad \forall v \in V \\
		\iff & c \circ \phi(x) = \phi(x) \circ c
		.
	\end{align*}
	Seien \(x \in L\) und \(a,b \in K^{n \times n}\), so dass
	\[
		[x,x_i] = \sum_{j=1}^{n} a_{i,j} \cdot x_{j}
		\qquad 
		[x,y_i] = \sum_{j=1}^{n} b_{i,j} \cdot y_{j}
	\]
	gilt, dann folgt
	\begin{align*}
		a_{i,k}
		&= \sum_{j=1}^{n} a_{i,j} \cdot \Kronecker{j}{k}
		= \sum_{j=1}^{n} a_{i,j} \cdot \beta(x_j,y_k)
		= \beta([x,x_i], y_k) \\
		&= - \beta(x_i, [x,y_k])
		= - \sum_{j=1}^{n} b_{k,j} \cdot \beta(x_i,y_j)
		= - \sum_{j=1}^{n} b_{k,j} \cdot \Kronecker{i}{j}
		= - b_{k,i}
		.
	\end{align*}
	Weiterhin gilt für \(x, y, z \in \gl{V}\)
	\begin{align*}
		[x,yz]
		= xyz - yzx
		= axy - yxz - yzx + yxz
		= [x,y]z - y[z,x]
		= [x,y]z + y[x,z]
		.
	\end{align*}
	Also ist
	\begin{align*}
		[\phi(x),c]
		&= \sum_{i=1}^{n} [\phi(x), \phi(x_i) \circ \phi(y_j)] \\
		&= \sum_{i=1}^{n} [\phi(x), \phi(x_i)] \circ \phi(y_j) + \sum_{i=1}^{n} \phi(x_i) \circ [\phi(x), \phi(y_j)] \\
		&= \sum_{i,j=1}^{n} \phi(a_{i,j} \cdot x_j) \circ \phi(y_i) + \sum_{i,j=1}^{n} \phi(x_i) \circ \phi(b_{i,j} \cdot y_j) \\
		&= \sum_{i,j=1}^{n} a_{i,j} \cdot \phi(x_j) \circ \phi(y_i) + \sum_{i,j=1}^{n} b_{j,i} \cdot \phi(x_j) \circ \phi(y_i) \\
		&= 0,
	\end{align*}
	da \(a_{i,j} = - b_{j,i}\) für alle \(1 \leq i,j \leq n\) gilt.
\end{beweis}
Damit wir obigen Endomorphismus stets konstruieren können, verallgemeinern wir die Killing-Form auf beliebige (statt \(\Ad\)) Darstellungen.
\begin{bemerkung}
	\label{thm:casimir-bilinearform}
	Sei \(K\) algebraisch abgeschlossener Körper der Charakteristik \(0\), \(L\) halbeinfach und \(\Kernel{\phi} = 0\).
	Dann ist
	\[ \beta : L \times L \to K, ~ (x,y) \mapsto \Trace(\phi(x) \circ \phi(y)) \]
	symmetrische, assoziative und nicht-ausgeartete Bilinearform.
\end{bemerkung}
\begin{beweis}
	Symmetrie und Assoziativität lassen sich wie schon bei der Killing-Form nachrechnen.
	Sei \(R \coloneq \Rad{\beta}\), dann ist \(\phi(R) \SubLieAlg \gl{V}\) auflösbar nach \cref{thm:cartans-criterion}.
	Wegen der Injektivität von \(\phi\) ist \(R \LieIso \phi(R)\), weshalb auch \(R\) nach \cref{thm:solvables}.1\ auflösbar ist.
	Ferner gilt \(R \Ideal L\), denn \(\beta\) ist assoziativ und mit \(x, y \in L, r \in R\) folgt
	\[ \beta([x,r],y) = - \beta([r,x],y) = - \beta(r,[x,y]) = 0 ,\]
	also \([x,r] \in R\).
	Weil \(L\) halbeinfach ist, ist das einzige auflösbare Ideal \(R = 0\).
\end{beweis}
\begin{definition}
	\label{def:casimir-element}
	Sei \(K\) algebraisch abgeschlossener Körper der Charakteristik \(0\), \(L \neq 0\) halbeinfach, \(\phi \neq 0\) und \(I \coloneq \Kernel{\phi}\).
	Nach \cref{thm:characterisation-of-semisimple-lie-algebras} ist \(L = L' \oplus I\), wobei \(L'\), \(I\) halbeinfach sind.
	Mit den Bezeichnungen aus \cref{thm:casimir-element}:
	Sei \(\beta'\) zu \(\RestrSrc{\phi}{L'}: L' \to \gl{V}\) wie in \cref{thm:casimir-bilinearform}, dann heißt
	\[ \Casimir \coloneq \Casimir[\RestrSrc{\phi}{L'}](\beta') \]
	das \Def{Casimir-Element} von \(\phi\).
\end{definition}
\begin{bemerkung}
	\label{thm:base-independence-of-casimir-element}
	Mit den Vorraussetzungen, wie in \cref{def:casimir-element}, gilt:
	\begin{enumerate}
		\item \(\Casimir \in \End[L]{V}\)
		\item \(\Trace(\Casimir) = \Dim{L/\Kernel{\phi}}\)
		\item Sei \(\Kernel{\phi} = 0\) und \(V\) einfacher \(L\)-Modul, dann ist
			\[ \Casimir = (\Dim{L} / \Dim{V}) \cdot \Id[V] .\]
	\end{enumerate}
	Insbesondere ist \(\Casimir[\phi] \coloneq \Casimir[\phi](\beta)\) unabhängig von Basen.
\end{bemerkung}
\begin{beweis}
	\begin{enumerate}
		\item Wie zuvor sei \(L = L' \oplus I\) mit \(I \coloneq \Kernel{\phi}\).
			Dann ist \(\Casimir \in \End[L']{V}\) nach \cref{thm:casimir-element}.
			Weil \(I\) aber trivial operiert, ist \(\Casimir\) sogar \(L\)-linear.
		\item Mit der Notation aus \cref{def:casimir-element} und \cref{thm:casimir-element}, \cref{thm:casimir-bilinearform} bezüglich \(L'\) ist:
			\begin{align*}
				\Trace(\Casimir)
				&= \Trace(\Casimir[\RestrSrc{\phi}{L'}](\beta')) \\
				&= \sum_{i=1}^{n} \Trace(\phi(x_i) \circ \phi(y_i)) \\
				&= \sum_{i=1}^{n} \beta'(x_i, y_i) \\
				&= \sum_{i=1}^{n} 1
				= \Dim{L'}
			\end{align*}
		\item Da \(V\) einfach ist, gilt nach \cref{thm:schurs-lemma} \(\Casimir = a \cdot \Id[V]\).
			Nach (2) ist aber
			\[ \Dim{L} = \Trace(\Casimir) = a \cdot \Dim{V} .\]
	\end{enumerate}
\end{beweis}
Um diese ganzen Sachverhalte zu verdeutlichen, ein
\begin{beispiel}
	Seien dazu \(L = \sln[2]{\Cmplx}\) die Lie-Algebra, \(V = \Cmplx^2\) der \(L\)-Modul und \(\phi: L \to \gl{V}\) die Einbettung.
	Mit
	\[
		x = \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix}
		\qquad
		y = \begin{pmatrix} 0 & 0 \\ 1 & 0 \end{pmatrix}
		\qquad
		h = \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}
	\]
	sei \(\beta(a,b) \coloneq \Trace(\phi(a), \phi(b))\) für \(a,b \in L\).
	Wir erhalten die bez.\ \(\beta\) duale Basis \((y,x,h/2)\) zu \((x,y,h)\).
	Daher ist
	\[ \Casimir = xy + yx + h^2/2 = \begin{pmatrix} \frac{3}{2} & 0 \\ 0 & \frac{3}{2} \end{pmatrix} ,\]
	wobei \(3/2 = \Dim{L} / \Dim{V}\) ist.
\end{beispiel}

\section{Der Satz von Weyl}
Sei \(K\) algebraisch abgeschlossener Körper der Charakteristik \(0\) und \(L\) endlich-dimensionale Lie-Algebra über \(K\).

\begin{bemerkung}
	\label{thm:image-of-representation}
	Sei \(L\) halbeinfach und \(\phi: L \to \gl{V}\) eine Darstellung, dann ist \(\Image{\phi} \SubLieAlg \sl{V}\).
	Insbesondere, wenn \(\Dim{V} = 1\) ist, ist \(\Image{\phi} = 0\).
\end{bemerkung}
\begin{beweis}
	Wegen \cref{thm:semisimple-lie-algebras} ist \(\LieCommut{L} = L\), daher folgt
	\[ \phi(L) = \phi(\LieCommut{L}) = \LieCommut{\phi(L)} \SubLieAlg \LieCommut{\gl{V}} = \sl{V} .\]
	Schließlich, falls \(\Dim{V} = 1\), ist \(\sl{V} \LieIso \sln[1]{K} = 0\).
\end{beweis}
Wir können nun \cref{thm:characterisation-of-semisimple-lie-algebras} verallgemeinern.
Das nachstehende Resultat mag intuitiv wirken, benötigt allerdings einiges an Beweisaufwand.
\begin{satz}[Weyl]
	\label{thm:weyls-theorem}
	Sei \(L\) halbeinfach und \(V\) ein endlich"=dimensionaler \(L\)-Modul, dann ist \(V\) halbeinfach.
\end{satz}
\begin{beweis}
	Sei \(W \SubLieMod V\) ein \(L\)-Untermodul.
	Entsprechend \cref{thm:semisimple-module-criterion} müssen wir die Existenz eines \(W\)-Komplement zeigen.
	Dazu sei \(\phi\) Darstellung zur \(L\)-Operation auf \(V\) und \(c \coloneq \Casimir\) das Casimir-Element von \(\phi\).

	\textbf{Spezialfall:}
		Wenn \(\Dim{V/W} = 1\), dann gilt \(V = W \oplus X\) mit \(X \in \LieModules\).

	\textbf{Beweis des Spezialfalls.}
		Wir führen Induktion über \(\Dim{V}\), unterscheide dazu die Fälle, ob \(W\) echte Untermoduln besitzt:
		\begin{itemize}
			\item \(0 \neq W\) ist einfach.
				Dann ist \(\Restr{c}{W}{W} = a \cdot \Id[W]\) für ein \(a \in K\) nach \cref{thm:schurs-lemma}.

				Wir erhalten die induzierte Darstellung
				\[ \varphi : L \to \gl{V/W}, ~ x \mapsto (v + W \mapsto \phi(x)(v) + W) \]
				auf dem Quotienten-Modul \(V/W\).
				Wegen \cref{thm:image-of-representation} gilt nach Voraussetzung also \(\Image{\varphi} = 0\).
				Folglich ist \(\phi(x)(V) \SubLieMod W\) für alle \(x \in L\).
				Daher ist nach der Definition des Casimir-Elements \(c(V) \SubLieMod W \PrpSubLieMod V\) und es folgt \(\Kernel{c} \neq 0\).
				Nun gilt
				\[ \Dim{L/\Kernel{\phi}} = \Trace(c) = a \cdot \Dim{W} \]
				nach \cref{thm:base-independence-of-casimir-element}.2.
				Es muss entweder \(a \neq 0\) oder \(\phi = 0\) gelten.
				Aus letzterem folgt die Behauptung, da wir \(V\) beliebig als direkte Summe von eindimensionalen \(L\)-Moduln schreiben können.
				Andernfalls, wenn \(a \neq 0\) ist, folgt aus \(\Kernel{\Restr{c}{W}{W}} = \Kernel{(a \cdot \Id[W])} = 0\), dass \(V = W \oplus \Kernel{c}\).
			\item Sei \(0 \neq W' \PrpSubLieMod W\) ein \(L\)-Untermodul.
				\(W/W'\) hat Kodimension \(1\) in \(V/W'\) und nach Induktions-Voraussetzung ist
				\[ V/W' = W/W' \oplus \tilde{W}/W' .\]
				Ferner gilt dann \(\Dim{\tilde{W}/W'} = 1\) und nach Induktions-Voraussetzung ist \(\tilde{W} = W' \oplus X\) als \(L\)-Moduln.
				Also haben wir \(V = W \oplus X\).
		\end{itemize}

	\textbf{Allgemein:}
		Wir wollen das Komplement wieder als einen Kern finden, dazu wird ein geeigneter Morphismus \(f\) benötigt.
		Die Idee ist, eine geeignete Zerlegung der Homomorphismen zu nutzen, so dass genau \(W\) unter \(f\) invariant und der Spezialfall anwendbar ist:

		Sei \(H \coloneq \Hom{V}{W}\) der \(L\)-Modul aus \cref{thm:lie-hom-modules}.2.
		Setze nun
		\begin{align*}
			H_1 &\coloneq \{ f \in H : \RestrSrc{f}{W} = a \cdot \Id[W] \text{ für ein } a \in K \} \\
			H_0 &\coloneq \{ f \in H : \RestrSrc{f}{W} = 0 \} ,
		\end{align*}
		dann ist offenbar \(H_0 \SubLieMod H_1 \SubLieMod H\) und \(\Dim{H_1/H_0} = 1\).
		Aus dem Spezialfall folgt nun \(H_1 = H_0 \oplus \VecSpan{f}\) mit \(\RestrSrc{f}{W} = \Id[W]\).
		Wir erhalte die Einschränkung der \(L\)-Operation auf \(\VecSpan{f}\) und die zugehörige Darstellung
		%\[ \varphi : L \to \gl{\VecSpan{f}}, ~ x \mapsto \RestrSrc{\phi(x)}{\VecSpan{f}} = (af \mapsto x \cdot af) \]
		\[ \varphi: x \mapsto \RestrSrc{\phi(x)}{\VecSpan{f}} = (af \mapsto x \cdot af) \]
		Nach \cref{thm:image-of-representation} ist \(\varphi\) trivial bzw. \(x \cdot f = 0\) für alle \(x \in L\).
		Folglich ist \(f\) wegen \cref{thm:lie-hom-modules} \(L\)-linear:
		\[ 0 = (x \cdot f)(v) = x \cdot f(v) - f(x \cdot v) .\]
		Also gilt \(f \in \Hom[L]{V}{W}\) und \(W \cap \Kernel{f} = 0\).
		Schließlich ist nach dem Rangsatz \(V =  W \oplus \Kernel{f}\) in \(\LieModules\).
\end{beweis}

\section{Abstrakte Jordan"=Chevalley"=Zerlegung}
Sei \(L\) endlich-dimensionale halbeinfache Lie-Algebra über dem algebraisch abgeschlossenen Körper \(K\) der Charakteristik \(0\) und \(V\) ein endlich"=dimensionaler \(K\)-Vektorraum.

Wir möchten die Jordan"=Chevalley"=Zerlegung von Unteralgebren der \(\gl{V}\) auf allgemeine Lie"=Algebren ausweiten.
Dazu wird eine Darstellung betrachtet, geeignet ist die \textit{adjungierte Darstellung}.
Wir erhalten so die zugehörige Zerlegung des dargestellten Elements in halbeinfachen und nilpotenten Anteil.
Zunächst einmal müssen wir dazu einsehen, dass die beiden Anteile tatsächlich im Bild der Darstellung liegen, wir wissen lediglich dass es sich um Elemente in \(\gl{L}\) handelt -- gerade hierfür benötigen wir einige Tricks und den Satz von Weyl.
Diese abstrakte Zerlegung wird für spätere Beweise sehr nützlich sein und stellt ein allgemeines Werkzeug im Umgang mit Lie"=Algebren dar.

\begin{satz}
	\label{thm:gl-jordan-chevalley-decomposition}
	Sei \(L \SubLieAlg \gl{V}\) und \(x = x_s + x_n\) die Jordan"=Chevalley"=Zerlegung zu \(x \in L\) (vgl. \cref{thm:jordan-chevalley-decomposition}), dann sind \(x_s, x_n \in L\).
\end{satz}
\begin{beweis}
	Betrachte \(V\) als \(\gl{V}\)-Modul mit Operation \(x \cdot v \coloneq x(v)\) für \(x \in L, v \in V\) und insbesondere dadurch als \(L\)-Modul.
	Für \(W \SubLieMod V\) als \(L\)-Untermodul sei
	\[ L_W \coloneq \{ x \in \gl{V} : x(W) \subseteq W \quad\text{und}\quad \Trace(\Restr{x}{W}{W}) = 0 \} \]
	Dann ist \(L \SubLieAlg L_W\) für alle \(W \SubLieMod V\) nach \cref{thm:image-of-representation} bezüglich der Darstellung \(\Id[L]\).
	Setze nun
	\[ N \coloneq \Normalizer[\gl{V}]{L}, \qquad L \SubLieAlg L' \coloneq N \cap \bigcap_{W \SubLieMod V} L_W .\]
	\begin{enumerate}[label=\textbf{Schritt \arabic*:}, align=left]
	\item Es gilt \(x_s, x_n \in L'\).

		\textbf{Beweis.}
		Nach \cref{thm:ad-jordan-chevalley-decomposition} ist \(\Ad~x = \Ad~x_s + \Ad~x_n\).
		Mit \cref{thm:jordan-chevalley-decomposition}.3 bezüglich \(\Ad~x\) sind daher \(x_s, x_n \in N\), denn
		\[
			[x_s,L] = \Ad~x_s(L) \SubLieAlg L
			\quad\text{ und }\quad
			[x_n,L] = \Ad~x_n(L) \SubLieAlg L
			.
		\]
		Ferner sind wegen
		\[ \Trace(\Restr{x_s}{W}{W}) = \underbrace{\Trace(\Restr{x}{W}{W})}_{= 0} - \underbrace{\Trace(\Restr{x_n}{W}{W})}_{= 0 \text{, da \(x_n\) nilpotent}} = 0 ,\]
		auch \(x_s, x_n \in L_W\) für alle \(W \SubLieMod V\).

	\item Es gilt \(L' = L\).

		\textbf{Beweis.}
		\(L'\) ist \(L\)-Modul vermöge \(\Ad_{L'}: L \to \gl{L'}\).
		Nach \cref{thm:weyls-theorem} ist \(L' = L \oplus Y\) mit \(L\)-Untermodul \(Y\).
		Da \(Y \SubLieMod L' \SubLieMod N\), ist \([L,Y] \SubLieAlg Y \cap L = 0\).
		
		Zerlege mit \cref{thm:weyls-theorem} nun \(V = W_1 \oplus \cdots \oplus W_s\), so dass alle \(W_i \SubLieMod V\) je einfach sind.
		Sei \(W \in \{ W_1, \ldots, W_s \}\) und \(y \in Y\).
		Dann ist \(y(W) \SubLieMod W\) und \(\Restr{y}{W}{W} \in \End[L]{W}\) wegen
		\[ x \cdot (y \cdot w) = y \cdot (x \cdot w) + \underbrace{[x,y]}_{\in L \cap Y = \NullSpace} \cdot w \]
		für \(x \in L\).
		Mit \cref{thm:schurs-lemma}, da \(y \in L'\), folgt nun \(\Restr{y}{W}{W} = 0\).
		Also ist \(y = 0\), da \(V = W_1 \oplus \cdots \oplus W_s\).
	\end{enumerate}
\end{beweis}
\begin{bemerkung}
	\label{thm:abstract-jordan-chevalley-decomposition}
	Zu \(x \in L\) existieren eindeutig bestimmte \(x_s, x_n \in L\) mit \(x = x_s + x_n\), \([x_s, x_n] = 0\), \(\Ad~x_s\) halbeinfach und \(\Ad~x_n\) nilpotent.
	Dies ist die \textit{abstrakte} \Def{Jordan"=Chevalley"=Zerlegung}.
\end{bemerkung}
\begin{beweis}
	Der \cref{thm:gl-jordan-chevalley-decomposition} für \(\Ad~L \SubLieAlg \gl{L}\) liefert
	\[ \Ad~x = \underbrace{\Ad~x_s}_{\in \Ad~L} + \underbrace{\Ad~x_n}_{\in \Ad~L} \]
	Da \(\Ad: L \to \gl{L}\) injektiv ist, erhalten wir eindeutige \(x_s, x_n \in L\).
\end{beweis}
\begin{korollar}
	Sei \(L \SubLieAlg \gl{V}\) und \(x \in L\).
	Dann ist die abstrakte Jordan"=Chevalley"=Zerlegung von \(x\) in \(L\) gleich der Jordan-Chevalley-Zerlege von \(x\) in \(\gl{V}\).
\end{korollar}
\begin{beweis}
	Folgt aus \cref{thm:ad-jordan-chevalley-decomposition} und \cref{thm:gl-jordan-chevalley-decomposition}.
\end{beweis}
\begin{korollar}
	\label{thm:representation-jordan-chevalley-decomposition}
	Sei \(\phi: L \to \gl{V}\) Darstellung und \(x \in L\) mit zugehöriger Jordan"=Chevalley"=Zerlegung \(x = x_s + x_n\).
	Dann ist \(\phi(x_s) + \phi(x_n)\) die Jordan"=Chevalley"=Zerlegung von \(\phi(x)\).
	Insbesondere ist \(\phi(x_s)\) halbeinfach und \(\phi(x_n)\) nilpotent.
\end{korollar}
\begin{beweis}
	\(\Ad~x_s \in \gl{L}\) ist halbeinfach nach \cref{thm:abstract-jordan-chevalley-decomposition}.
	Folglich besitzt \(L\) eine Eigenvektorbasis von \(\Ad~x_s\).
	Dann bestitzt aber auch \(L' \coloneq \phi(L)\) eine Eigenvektorbasis bezüglich \(\Ad_{L'}~\phi(x_s)\).
	Damit ist \(\Ad_{L'}~\phi(x_s)\) halbeinfach in \(\End{L'}\).
	Klar ist, dass \(\phi(x_n)\) \(\Ad_{L'}\)-nilpotent und \([\phi(x_s), \phi(x_n)] = 0\) ist.
	Also ist \(\phi(x_s) + \phi(x_n)\) die abstrakte Jordan"=Chevalley"=Zerlegung von \(\phi(x)\) und damit gleich der Jordan"=Chevalley"=Zerlegung.
\end{beweis}

\section{Die endlich-dimensionalen Darstellungen von \ensuremath{\mathfrak{sl}_2(K)}}
\label{section:finite-representation-of-sl2K}
Sei \(K\) algebraisch abgeschlossener Körper der Charakteristik \(0\).
Wir betrachten nun die Lie-Algebra \(L \coloneq \sln[2]{K}\).
In diesem Abschnitt seien dazu alle \(K\)-Vektorräume und \(L\)-Moduln endlich"=dimensional.
Sei weiterhin im folgenden \(V \in \LieModules\) ein \(L\)-Modul, \(\lambda \in K\) und \(\WeightSpace{V}{\lambda} \coloneq \{ v \in V : h \cdot v = \lambda v \}\).

Wir haben die Basis von \(L\) durch die Elemente
\begin{align*}
	x \coloneq \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix}
	\qquad
	y \coloneq \begin{pmatrix} 0 & 0 \\ 1 & 0 \end{pmatrix}
	\qquad
	h \coloneq \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}
	.
\end{align*}
Um lästige Rechnerrei zu vermeiden berechnen wir
\begin{align}
	\label{equation:sl2-base-brackets}
	[h,x] = 2x,
	\qquad
	[h,y] = -2y,
	\qquad
	[x,y] = h.
\end{align}

\textit{An dieser Stelle begann Herr Kehrle zu lachen. Herr Professor Hiß war darüber verwirrt und dachte, es läge an obigen trivialen Produkten. Diese seien doch recht nützlich zu wissen! Tatsächlich lachte er allerdings, da sein Sitznachbar ihn darauf hinwies, dass direkt davor eine ungültige Vorraussetzung an die Tafel geschrieben wurde: So sollten alle (und nicht nur die betrachteten) \(K\)-Vektorräume endlich"=dimensional sein. Dies würde ja den ganzen Unterabschnitt nutzlos gestalten! Herr Kehrle wollte sich zu diesem Vorfall allerdings nicht weiter äußern.}

\begin{lemma}
	Es existieren \(\lambda_1, \ldots, \lambda_m \in K\) mit \(V = \bigoplus_{i = 1}^{m} \WeightSpace{V}{\lambda_i}\).
\end{lemma}
\begin{beweis}
	Sei \(\phi: L \to \gl{V}\) zugehörige Darstellung.
	Weil \(h \in \gln[2]{K}\) halbeinfach ist, ist auch \(\phi(h) \in \gl{V}\) halbeinfach nach \cref{thm:representation-jordan-chevalley-decomposition}.
	Daraus ergibt sich die direkte Summe.
\end{beweis}
\begin{definition}
	Die \(\WeightSpace{V}{\lambda} \neq 0\) nennen wir \Def{Gewichtsraum} und je das zugehörige \(\lambda\) ein \Def{Gewicht} von \(h\) in \(V\).
\end{definition}
Die Namensgebung sei wie folgt zu verstehen:
Wir werden sehen, dass sich ein bezüglich der Gewichte maximaler Vektor finden lässt.
Dieser ist besonders \emph{gewichtend}.
\begin{lemma}
	\label{thm:raising-and-lowering}
	Für \(v \in V_{\lambda}\) gilt
	\[
		x \cdot v \in V_{\lambda + 2},
		\qquad
		y \cdot v \in V_{\lambda - 2}.
	\]
\end{lemma}
\begin{beweis}
	Es gilt
	\begin{align*}
		x \cdot v
		= [h,x] \cdot v + x \cdot (h \cdot v)
		= 2x \cdot v + \lambda x \cdot v
		= (\lambda + 2)(x \cdot v)
	\end{align*}
	und analoges für \(y\).
\end{beweis}
\begin{bemerkung}
	Es sei \(V = \bigoplus_{\lambda \in K,\\V_{\lambda} \neq 0} V_{\lambda}\) und ferner \(K\) der Charakteristik \(0\).
	Betrachte \(V_{\lambda}, V_{\lambda + 2}, V_{\lambda + 4}, \ldots\) bzw. \(V_{\lambda}, V_{\lambda - 2}, V_{\lambda - 4}, \ldots\) für alle \(\lambda \in K\) mit \(V_{\lambda} \neq 0\).
	Dann existiert ein \(\lambda \in K\) mit \(V_{\lambda} \neq 0\)und \(V_{\lambda + 2} = 0\).
	Mit diesem \(\lambda\) gilt \(x . v = 0\) für \(v \in V_{\lambda}\).
\end{bemerkung}
\begin{definition}
	Sei \(V \in \LieModules\), \(\lambda \in K\) mit \(V_{\lambda} \neq 0\) und \(V_{\lambda + 2} = 0\).
	\(0 \neq v \in V_{\lambda}\) heißt \Def{maximaler Vektor} von \(V\).
\end{definition}
\begin{lemma}
	\label{thm:base-products}
	Sei \(V \in \LieModules\) und \(v_0 \in V_{\lambda}\) maximaler Vektor.
	Setze
	\[
		v_{-1} \coloneq 0
		\qquad
		v_i \coloneq \frac{1}{i!} \cdot y^i \cdot v_0 ,
	\]
	dann gilt mit \(i \leq 0\):
	\begin{enumerate}
		\item \(h \cdot v_i = (\lambda - 2i) v_i\)
		\item \(y \cdot v_i = (i + 1) v_{i + 1}\) 
		\item \(x \cdot v_i = (\lambda - i + 1) v_{i-1}\)
	\end{enumerate}
\end{lemma}
\begin{beweis}
	\begin{enumerate}
		\item Iteriere \cref{thm:raising-and-lowering}.
		\item Folgt aus der Definition mit \([y,y] = 0\).
		\item Induktion über \(i\):
			Für \(i = 0\) ist die Behauptung klar wegen \(v_{-1} = 0\).
			Nun ist, wenn die Aussage für \(i\) gilt:
			\begin{alignat*}{2}
				(i + 1) x \cdot v_{i + 1}
				&= x \cdot (y \cdot v_i) \\
				&= [x,y] \cdot v_i + y \cdot (x \cdot v_i) \\
				&= h \cdot v_i + y \cdot (x \cdot v_i) \\
				&= (\lambda - 2i) v_i + (\lambda - i + 1) y \cdot v_{i - 1} && \qquad \text{nach (1) mit Induktion} \\
				&= (\lambda - 2i) v_i + i (\lambda - i + 1) v_i && \qquad \text{nach (2)} \\
				&= (i + 1) (\lambda - i) v_i.
			\end{alignat*}
	\end{enumerate}
\end{beweis}
\begin{satz}
	\label{thm:characterization-of-simple-L-modules}
	Sei \(V\) endlich"=dimensionaler einfacher \(L\)-Modul.
	Dann ist
	\begin{enumerate}
		\item Sei \(m \coloneq \dim{V} - 1\).
			Die Gewichte von \(V\) sind \(m, m-2, \ldots, -m\).
			Ferner ist \(\Dim{V_{\mu}} = 1\) für alle Gewichte \(\mu\).
		\item \(V\) besitzt (bis auf skalare Vielfache) genau einen maximalen Vektor.
			Dieser hat Gewicht \(m\).
		\item Zu \(n \in \Nats\) existiert höchstens ein einfacher \(L\)-Modul (bis auf Isomorphie) der Dimension \(n + 1\).
	\end{enumerate}
\end{satz}
\begin{beweis}
	\begin{enumerate}[align=left]
		\item[(1),(2):]
			Sei \(v_0 \in V_{\lambda}\) maximaler Vektor und
			\[ m \coloneq \min \{ k \in \Nats : v_{i} \neq 0 \text{ und } v_{i+1} = 0 \} ,\]
			mit \(v_i\) wie in \cref{thm:base-products}.
			Die Vektoren \(v_0, v_1, \ldots, v_m\) sind linear unabhängig nach \cref{thm:base-products}.1, da \(v_i \in V_{\lambda - 2i}\).
			Weiter ist \(\VecSpan{v_0, v_1, \ldots, v_m} \SubLieMod V\) ein \(L\)-Untermodul.
			Wegen \(\Dim{V} = m + 1\) folgt \(V = \VecSpan{v_0, v_1, \ldots, v_m}\) und \(\Dim{V_{\mu}} = 1\) für alle Gewichte \(\mu\).
			Die Gewichte sind \(\lambda - 2i\) für \(i = 0, \ldots, m\).
			Aus \cref{thm:base-products}.3 folgt \(\lambda = m\) mit \(i = m + 1\) (\(0 = x \cdot v_{m+1} = (\lambda - m)v_m\)).
		\item[(3)]
			Die Matrizen von \(x, y, h\) bezüglich der Basis \(v_0, \ldots, v_m\) sind durch die Formeln in \cref{thm:base-products} mit \(\lambda = m\) festgelegt.
	\end{enumerate}
\end{beweis}
\begin{bemerkung}
	\begin{enumerate}
		\item Sei \(V\) einfacher, endlich"=dimensionaler \(L\)-Modul mit \(\Dim{V} = m + 1\) und Basis \(\{ v_0, \ldots, v_m \}\) wie im Beweis von \cref{thm:characterization-of-simple-L-modules}.
			Dann sind die Matrizen von \(x, y, h\) bezüglich dieser Basis obere bzw. untere Dreiecksmatrizen bzw. eine Diagonalmatrix.
		\item Für jedes \(m \in \Nats\) existiert ein einfacher \(L\)-Modul der Dimension \(m + 1\).
	\end{enumerate}
\end{bemerkung}
\begin{beweis}
	\textcolor{red}{Übung}.
\end{beweis}
\begin{korollar}
	\label{thm:h-eigenvalues-are-integers}
	Sei \(V\) endlich"=dimensionaler \(L\)-Modul.
	Dann liegen die Eigenwerte von \(h\) auf \(V\) in \(\Ints\).
	Ist \(k\) Eigenwert, dann auch \(-k\).
	Ist \(V = W_1 \oplus \cdots \oplus W_s\), wobei jedes \(W_i\) einfach ist, dann gilt \(s = \Dim{\WeightSpace{V}{0}} + \Dim{\WeightSpace{V}{1}}\) (dabei sind \(\WeightSpace{V}{0}, \WeightSpace{V}{1}\) die Gewichtsräume von \(V\) zu dem Gewicht \(0\) bzw. \(1\)).
\end{korollar}
\begin{beweis}
	Nach \cref{thm:characterization-of-simple-L-modules} besitzt jedes \(W_i\) einen eindimensionalen Gewichtsraum entweder zum Gewicht \(0\) oder zum Gewicht \(1\) (aber nicht zu beiden).
\end{beweis}
